# SequelBase64Upload

[![Coverage](/coverage.svg)]()

This gem handles base64 encoded data and saves the decoded binary file to desired place. Amazon S3 and File system is supported.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'sequel_base64_upload'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install sequel_base64_upload

## Usage

Create a table for the binary data

```ruby
    create_table(:uploads) do
        primary_key :id
        varchar     :file_path
        integer     :read_count
        datetime    :last_read_at
    end
```

Create your sequel model and initilize the plugin with the provider

```ruby
    class Upload < Sequel::Model
      plugin :sequel_base64_upload,provider: :file_system, 
          provider_opts: {tmp_dir: ROOTPATH.join('tmp'),data_dir: ROOTPATH.join('data')}
      def before_save
        self.file_path = write
      end
    end
```
Create an object

```ruby
upload = Upload.new(base64_data: image_file_data)
```




## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing
1. Fork it ( http://github.com/planas/sequel_enum/fork )
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create new Pull Request