# frozen_string_literal: true
require 'rubygems'
require 'bundler'
Bundler.setup
require 'simplecov'
require "simplecov_json_formatter"
# Generate HTML and JSON reports
SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new([
  SimpleCov::Formatter::HTMLFormatter,
  SimpleCov::Formatter::JSONFormatter])

require 'sequel'
require 'sequel/extensions/migration'
require 'base64'
require 'open-uri'
require 'net/http'

ROOTPATH = Pathname.new(File.expand_path('../../', __FILE__))

RSpec.configure do |config|
  
  config.before :suite do
    #Download sample files
    open(ROOTPATH.join('spec','fixtures','file_example_JPG_500kB.jpg'), 'wb') do |file|
      file << URI.parse('https://file-examples-com.github.io/uploads/2017/10/file_example_JPG_500kB.jpg').read
    end
    open(ROOTPATH.join('spec','fixtures','file-example_PDF_1MB.pdf'), 'wb') do |file|
      file << URI.parse('https://file-examples-com.github.io/uploads/2017/10/file-example_PDF_1MB.pdf').read
    end
    `zip #{ROOTPATH.join('spec','fixtures','file-example_zip.zip')} #{ROOTPATH.join('spec','fixtures','file-example_PDF_1MB.pdf')}` 
    FileUtils.mkdir_p ROOTPATH.join('tmp')
    Sequel::Model.db = Sequel.connect('sqlite://tmp/test.db')
    Sequel.migration do
      up do
        create_table(:uploads) do
          primary_key :id
          varchar     :file_path
          integer     :read_count,default: 0
          datetime    :last_read_at
        end
      end
    end.apply(Sequel::Model.db, :up)
    class Upload < Sequel::Model
      plugin :sequel_base64_upload,provider: :file_system, 
          provider_opts: {tmp_dir: ROOTPATH.join('tmp'),data_dir: ROOTPATH.join('data')},
          read_count_column: :read_count,
          last_read_at_column: :last_read_at
      def before_save
        self.file_path = write
      end
    end
    
  end
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
  config.after(:suite) do
    Sequel::Model.db.drop_table :uploads
  end
end

SimpleCov.minimum_coverage 90 
SimpleCov.start
SimpleCov.at_exit do
  op = SimpleCov.result.coverage_statistics[:line].percent
  color = case op
when 0..20 then :red
when 21..40 then :orange
when 41..60 then :yellow
when 61..80 then :yellowgreen
when 81..90 then :green
else :brightgreen
end
File.write("coverage.svg", Net::HTTP.get(URI.parse("https://img.shields.io/badge/coverage-#{op.round(2)}-#{color}.svg")))
SimpleCov.result.format!
end
require_relative '../lib/sequel_base64_upload'
