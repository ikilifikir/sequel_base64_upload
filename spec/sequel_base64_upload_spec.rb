# frozen_string_literal: true

RSpec.describe SequelBase64Upload do
  it "has a version number" do
    expect(SequelBase64Upload::VERSION).not_to be nil
  end
  
  describe "Sample files" do
    let(:image_file_data) { Base64.strict_encode64( File.read(ROOTPATH.join('spec','fixtures','file_example_JPG_500kB.jpg')) ) }
    let(:pdf_file_data) { Base64.strict_encode64( File.read(ROOTPATH.join('spec','fixtures','file-example_PDF_1MB.pdf')) ) }
    let(:compressed_file_data) { Base64.strict_encode64( File.read(ROOTPATH.join('spec','fixtures','file-example_zip.zip')) ) }
    it "image file should be saved with base64 data" do
      upload = Upload.new(base64_data: image_file_data)
      expect{upload.save}.not_to raise_error
    end
    it "pdf file should be saved with base64 data" do
      upload = Upload.new(base64_data: pdf_file_data)
      expect{upload.save}.not_to raise_error
    end
    it "compressed file should be saved with base64 data" do
      upload = Upload.new(base64_data: compressed_file_data,uncompress: true)
      expect{upload.save}.not_to raise_error
    end   
    it "should have meta data" do
      upload = Upload.first
      expect(upload.meta).to include(:file_size,:sha256_sum,:sha512_sum,:sha1_sum,:md5_sum,:mime_type)
    end
    it "should increase read count" do
      read_count_before = Upload.first.read_count
      Upload.first.read
      expect(Upload.first.read_count).to eq(read_count_before + 1)
    end
    it "should save last read time" do
      read_time = Upload.first.last_read_at
      sleep 1
      Upload.first.read
      expect(Upload.first.last_read_at).to be > read_time
    end
  end
  
end
