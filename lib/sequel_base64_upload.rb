# frozen_string_literal: true

require_relative "sequel_base64_upload/version"
require_relative "sequel_base64_upload/storage_providers/storage_providers"

module Sequel
  module Plugins
    module SequelBase64Upload
      
      def self.configure(model, opts = {})
        model.instance_eval do
          @provider = ::SequelBase64Upload::StorageProviders.provider(
            opts.fetch(:provider, :file_system),
            opts.fetch(:provider_opts, {}))
          @key_column     = opts.fetch(:key_column, :file_path)
          
          # Optional columns
          @read_count_column    = opts.fetch(:read_count_column, nil)
          @last_read_at_column  = opts.fetch(:last_read_at_column, nil)
          
        end
      end
      
      module ClassMethods
        attr_reader :provider,
        :key_column,
        :read_count_column,
        :last_read_at_column
      end
      
      module InstanceMethods
        attr_accessor :base64_data, :uncompress
        
        # Create and return file path
        def write
          model.provider.write(base64_data,uncompress)
        end
        def read
          model.provider.read(self.send(model.key_column))
          self.this.update(model.read_count_column => 1 ) if model.read_count_column
          self.this.update(model.last_read_at_column => Time.now ) if model.last_read_at_column
        end
        def meta
          model.provider.meta(self.send(model.key_column))
        end
                 
      end
      
    end
  end
end
  
