module SequelBase64Upload
  module Helpers
    def self.included base
      base.extend ClassMethods
    end
    module ClassMethods
      def check_create_dir(d)
        Dir.mkdir(d) unless Dir.exists?(d)
        Pathname.new(d)
      end
      def temp_file(dir)
        Tempfile.new('tmpf',dir)
      end
      def write_base64(base64_data,file_path)
        File.write(file_path,Base64.decode64(base64_data))
        file_path
      end
      def file_mime_type(f)
        `file --brief --mime-type #{f}`.gsub(/\n/,"")
      end
    end
  end
end
  
