module SequelBase64Upload
  class UnknownCompressionAlgorith < StandardError; end
  class InvalidCompressedFile < StandardError; end
end
