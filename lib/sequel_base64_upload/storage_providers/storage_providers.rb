require_relative  'file_system'

module SequelBase64Upload
  module StorageProviders
    def self.provider(prv,opts={})
      pcn =  prv.to_s.split("_").map{|e| e.capitalize}.join
      provider = Kernel.const_get("SequelBase64Upload::StorageProviders::#{pcn}")
      opts.each { |k,v| provider.public_send("#{k}=", v) }
      provider
    end
  end
end
