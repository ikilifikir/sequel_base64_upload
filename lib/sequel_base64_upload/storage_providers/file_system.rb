require 'securerandom'
require 'pathname'
require 'tempfile'
require 'fileutils'
require 'digest'
require_relative '../helpers'
require_relative '../uncompressor'

module SequelBase64Upload
  module StorageProviders
    class FileSystem
      include ::SequelBase64Upload::Helpers
      class << self
        attr_writer :dir_mask,:tmp_dir,:data_dir
        #dir_mask - subdirectory name mask for strftime
        def dir_mask
          @dir_mask ||= defaults[:dir_mask]
        end
        def tmp_dir
          @tmp_dir ||= defaults[:tmp_dir]
          check_create_dir(@tmp_dir)
        end
        def data_dir
          @data_dir ||= defaults[:data_dir]
          check_create_dir(@data_dir)
        end
        
        #writes data to file
        def write(base64_data,uncompress=false)
          b64d = base64_data
          if uncompress
            b64d = ::SequelBase64Upload::Uncompressor.uncompress(b64d,tmp_dir)
          end
          write_base64 b64d, file
        end
        
        def read(f)
          File.open(f).read
        end
        def read_base64(f)
          Base64.strict_encode64(read(f))
        end
        def meta(f)
          meta_data = {}
          meta_data[:file_size]   =  File.size(f)
          meta_data[:sha256_sum]  =  Digest::SHA256.file(f).to_s
          meta_data[:sha512_sum]  =  Digest::SHA512.file(f).to_s
          meta_data[:sha1_sum]    =  Digest::SHA1.file(f).to_s
          meta_data[:md5_sum]     =  Digest::MD5.file(f).to_s
          meta_data[:mime_type]   =  file_mime_type(f)
          meta_data
        end
        
        def defaults
          {
            data_dir: "data",
            tmp_dir: "tmp",
            dir_mask: "%Y-%m-%W"
          }.freeze
        end
        
        private
        def file
          sub_dir = Time.now.strftime(dir_mask)
          sub_dir = check_create_dir(data_dir.join(sub_dir).to_s)
          data_dir.join(sub_dir.to_s,SecureRandom.uuid)
        end
      end
    end
  end
end
