require_relative 'helpers'
require_relative 'error'
module SequelBase64Upload
  class Uncompressor
    include ::SequelBase64Upload::Helpers
    
    class << self
      def uncompress(base64_data,tmp_dir)
        raw_data_file = write_base64(base64_data,temp_file(tmp_dir))
        fmt = file_mime_type(raw_data_file.path)
        raise ::SequelBase64Upload::InvalidCompressedFile.new("Empty mime type returned") if fmt.to_s.empty? 
        case fmt
        when "application/x-bzip2"
        when "application/zip"
          uncompress_zip(raw_data_file)
        when "application/gzip"
        else
          raise ::SequelBase64Upload::UnknownCompressionAlgorith.new("Unimplemented algorithm #{fmt}") 
        end
      end
      
      private
      def uncompress_zip(f)
        require 'zip'
        extracted_file = ""
        Zip::File.open(f) do |zip_file|
          #raise ::SequelBase64Upload::InvalidCompressedFile.new("More then 1 file in archive") if zip_file.size > 1 
          entry = zip_file.first
          extracted_file = "#{f.path}_extracted"
          zip_file.extract(entry,extracted_file)
        end
        Base64.strict_encode64(File.read(extracted_file))
      end
      
    end
  end
end
