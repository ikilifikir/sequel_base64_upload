# frozen_string_literal: true

require_relative "lib/sequel_base64_upload/version"

Gem::Specification.new do |spec|
  spec.name          = "sequel_base64_upload"
  spec.version       = SequelBase64Upload::VERSION
  spec.authors       = ["Fatih GENÇ"]
  spec.email         = ["fatihgnc@gmail.com"]

  spec.summary       = "Handles file upload via base64 strings at model level"
  spec.description   = "Handles file upload via base64 strings at model level"
  spec.homepage      = "https://gitlab.com/ikilifikir/sequel_base64_upload"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.4.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = "https://gitlab.com/ikilifikir/sequel_base64_upload/-/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  
  spec.add_dependency             'sequel',      '~> 5.0','>= 5.0.0'
  spec.add_dependency             'rubyzip',     '~> 2.3','>= 2.0.0'
  spec.add_development_dependency 'rspec',       '~> 3.0'
  spec.add_development_dependency 'simplecov',   '~> 0.21'
  spec.add_development_dependency 'sqlite3',     '~> 1.4'
  
  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
